/**
 * Created by issam on 10/06/2017.
 */
import {NgModule} from '@angular/core';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { reducers} from '../store/index';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {RouterEffects} from './router/router.effects';
import { RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {CustomRouterStateSerializer} from './router/customRouterStateSerializer';

const NGRX_MODULES = [
    /**
     * StoreModule.provideStore is imported once in the root module, accepting a reducer
     * function or object map of reducer functions. If passed an object of
     * reducers, combineReducers will be run creating your application
     * meta-reducer. This returns all providers for an @ngrx/store
     * based application.
     */
  StoreModule.forRoot(reducers),
  /**
     * Store devtools instrument the store retaining past versions of state
     * and recalculating new states. This enables powerful time-travel
     * debugging.
     *
     * To use the debugger, install the Redux Devtools extension for either
     * Chrome or Firefox
     *
     * See: https://github.com/zalmoxisus/redux-devtools-extension
     * See: https://github.com/ngrx/store/issues/357#issuecomment-288822640
     */
    StoreDevtoolsModule.instrument({ maxAge: 50 }),
];
const EFFECTS_SETUPS = [
    /**
     * EffectsModule.forRoot() sets up the effects class to be initialized
     * immediately when the application starts.
     *
     * See: https://github.com/ngrx/effects/blob/master/docs/api.md#run
     */
    EffectsModule.forRoot([
      RouterEffects
    ]),

];
export const EFFECTS = [
    RouterEffects
];

@NgModule({
    imports: [
        NGRX_MODULES,
        EFFECTS_SETUPS,

      /**
       * @ngrx/router-store keeps router state up-to-date in the store.
       */
      StoreRouterConnectingModule,
    ],
    providers: [
        EFFECTS,
      { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer },
    ],
    declarations: [],
    exports: []
})
export class StoreAppModule {

}

