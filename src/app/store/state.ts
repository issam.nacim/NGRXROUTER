/**
 * Created by issam on 29/05/2017.
 * global state of app
 */
import * as fromRouter from '@ngrx/router-store';
import {Router} from './router/router.model';

export interface State {
  router: fromRouter.RouterReducerState<Router>;
}
