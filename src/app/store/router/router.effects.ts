import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Effect, Actions } from '@ngrx/effects';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import * as RouterActions from './router.action';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RouterEffects {

  constructor(private actions$: Actions,
              private router: Router,
              private location: Location) {}


/*
  @Effect()
  navigate$: Observable<Action> = this.actions$.ofType(RouterActions.GO)
                            .map((action: RouterActions.Go) => action.payload)
                            .do(({ path, query: queryParams, extras}) => this.router.navigate(path, { queryParams, ...extras }));
*/
  @Effect()
  navigateBack$: Observable<Action> = this.actions$.ofType(RouterActions.BACK)
                                .do(() => this.location.back());

  @Effect()
  navigateForward$: Observable<Action> = this.actions$.ofType(RouterActions.FORWARD)
                                  .do(() => this.location.forward());


}
