import {  Params } from '@angular/router';
export interface Router {
  url: string;
  queryParams: Params;
}
