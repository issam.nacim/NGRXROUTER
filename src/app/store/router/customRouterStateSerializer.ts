/**
 * Created by issam on 16/12/2017.
 */
import {RouterStateSerializer} from '@ngrx/router-store';
import {Router} from './router.model';
import {RouterStateSnapshot} from '@angular/router';

export class CustomRouterStateSerializer
  implements RouterStateSerializer<Router> {
  serialize(routerState: RouterStateSnapshot): Router {
    const { url } = routerState;
    const queryParams = routerState.root.queryParams;

    return { url, queryParams };
  }
}
