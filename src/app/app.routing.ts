import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';

// Import Containers

export const routes: Routes = [
  {
    path: 'Home',
    component: AppComponent,
    data: {
      title: 'Home'
    },

  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
